import os
import pandas as pd  # pip install pandas
from functions import DatabaseFunctions as Df
from functions import GeophysicalFunctions as Gf

# set db vars
pghost = "10.33.131.50"
pgport = "5432"
pgdatabase = 'pcgerda'
pguser = os.environ.get('DB_ADMIN_USER')
pgpassword = os.environ.get('DB_ADMIN_PASS')
schemaname = 'dataquality'
tablename = 'tem_missing_datapoints'

tem_datasets = Gf(pghost, pgport, pgdatabase, pguser, pgpassword)
df = pd.DataFrame(tem_datasets.find_coupled_tem(), columns=['Dataset'])
print(df)
Df.export_df_to_db(df, schemaname, tablename)

print('done')

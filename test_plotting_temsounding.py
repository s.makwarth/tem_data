import os
import matplotlib.pyplot as plt
from functions import DatabaseFunctions as Df

# set db vars
pghost = "10.33.131.50"
pgport = "5432"
pgdatabase = 'pcgerda'
pguser = os.environ.get('DB_ADMIN_USER')
pgpassword = os.environ.get('DB_ADMIN_PASS')
schemaname = 'pcgerda'

sql_file = './sql/get_tem_dbdt.sql'
sql = Df.load_sql(sql_file)

df_temwave = Df.import_db_to_df(sql)

unique_datasets = df_temwave['dataset'].unique()
# unique_datasets = Gf.find_coupled_tem(pghost, pgport, pgdatabase, pguser, pgpassword)
tem_grouped = df_temwave.groupby(['dataset', 'daposition', 'segment'])

for i, unique_dataset in enumerate(unique_datasets):
    fig = plt.figure(figsize=(10, 7))
    fig.suptitle('Dataset: '+str(int(unique_dataset)), fontsize=16)
    for name, group in tem_grouped:
        if name[0] == unique_dataset:
            group.sort_values(by=['sequence'])

            # error calculated as value*stdf and value/stdf
            try:
                error = (group['ordimeaval']*group['ordistdval'])-(group['ordimeaval']/group['ordistdval'])
            except:
                error = None

            # plot result with same colors for all plots
            ax = plt.gca()
            color = next(ax._get_lines.prop_cycler)['color']

            ax.plot(group['abscival'], group['ordiresval'], color=color)
            # plot std if defined in dataset
            if error is not None:
                ax.errorbar(group['abscival'], group['ordimeaval'], error, fmt='.', color=color, ecolor=color)
            else:
                ax.scatter(group['abscival'], group['ordimeaval'], color=color)
            ax.set_yscale('log')
            ax.set_xscale('log')

    # save figures
    plot_path = './plots/tem_error/'
    if not os.path.exists(plot_path):
        os.makedirs(plot_path)
    plt.savefig(plot_path + '{}.png'.format('Dataset_' + str(int(unique_dataset))))
    plt.savefig(plot_path + '{}.pdf'.format(unique_dataset))

    # close figure
    plt.close(fig)

print('done')

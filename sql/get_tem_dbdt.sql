    SELECT ofr.*
    FROM pcgerda.odvfwres ofr
    INNER JOIN pcgerda.dataset dat ON ofr.dataset = dat.dataset
    INNER JOIN pcgerda.temhea th ON ofr.dataset = th.dataset
    WHERE upper(dat."datatype") = 'TEM' 
	    AND upper(th.temsubtype) IN ('HMTEM1', 'HMTEM2', 'PATEM', 'TEM40', 'TEMCL', 'TEMOFF', 'TEMCO')
    ORDER BY ofr.model, ofr.dataset, ofr.moposition, ofr.daposition, ofr."sequence"
	;
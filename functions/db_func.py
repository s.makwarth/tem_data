# -*- coding: utf-8 -*-
__author__ = 'Simon Makwarth <simak@mst.dk>'


class DatabaseFunctions:
    def __init__(self, pghost, pgport, pgdatabase, pguser, pgpassword):
        """
        :param pghost: database host
        :param pgport: database port
        :param pgdatabase: database name
        :param pguser: database user
        :param pgpassword: database password
        """
        self.pghost = pghost
        self.pgport = pgport
        self.pgdatabase = pgdatabase
        self.pguser = pguser
        self.pgpassword = pgpassword

    def export_df_to_db(self, df, schemaname, tablename):
        """
        Export pandas df to postgresDB
        :param df: pandas dataframe
        :param schemaname: database schema to export dataframe to
        :param tablename: database table to export dataframe to
        :return: none
        """
        from sqlalchemy import create_engine  # pip install sqlalchemy
        try:
            engine = create_engine(
                r"postgresql+psycopg2://" + self.pguser + ":" + self.pgpassword + "@" + self.pghost + ":" + self.pgport + "/" + self.pgdatabase)
            df.to_sql(tablename, con=engine, schema=schemaname, if_exists='replace', index=False)
        except Exception as e:
            print('Table could not be edited: ' + self.pgdatabase + '.' + schemaname + '.' + tablename)
            print(e)

    def connect_to_db(self):
        """
        Connect to postgresql database
        :return: Postgresql Database connection
        """
        import psycopg2 as pg
        try:
            con = pg.connect(
                host=self.pghost,
                port=self.pgport,
                database=self.pgdatabase,
                user=self.pguser,
                password=self.pgpassword
            )
        except Exception as e:
            print('Unable to connect to database: ' + self.pgdatabase)
            print(e)
        else:
            return con

    def import_db_to_df(self, sql):
        """
        Imports sql query to pandas dataframe
        :param sql: sql string
        :return: pandas dataframe from sql query
        """
        import pandas as pd
        con = self.connect_to_db()
        try:
            imported_df = pd.read_sql(sql, con)
            return imported_df
        except Exception as e:
            print('sql could not be executed')
            print(e)
        DatabaseConnection.close_db_connection(con)


class DatabaseConnection:
    def __init__(self, con):
        """
        :param pghost: database host
        :param pgport: database port
        :param pgdatabase: database name
        :param pguser: database user
        :param pgpassword: database password
        """
        self.con = con

    def close_db_connection(self):
        """
        Closes a database connection
        :return: none
        """
        try:
            self.con.close
        except Exception as e:
            print('WARNING: Database could not be closed')
            print(e)

    def execute_sql(self, sql_path, sql):
        """
        Execute sql to database connection either from sql or sql-file
        :param con: postgres database connection
        :param sql_path: path to .sql file
        :param sql: inset sql directly in python
        """
        if sql_path != '' and sql == '':
            sql_file = open(sql_path, 'r')
            cur = self.con.cursor()
            cur.execute(sql_file.read())
            self.con.commit()
            cur.close()
        elif sql_path == '' and sql != '':
            cur = self.con.cursor()
            cur.execute(sql)
            self.con.commit()
            cur.close()
        else:
            print('ERROR: insert either sql_path OR sql')

    @staticmethod
    def load_sql(sql_file):
        """
        Transform sql query from sql-file to string
        :param sql_file: path to sql file
        :return: sql string
        """
        fd = open(sql_file, 'r')
        sql = fd.read()
        fd.close()
        return sql

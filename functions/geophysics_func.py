"""
    Desc: loop through tem dataset and find soundings with missing data-points
    if times between data-points is not increasing
"""

__author__ = 'Simon Makwarth <simak@mst.dk>'


class GeophysicalFunctions:
    def __init__(self, pghost, pgport, pgdatabase, pguser, pgpassword):
        """
        :param pghost: database host
        :param pgport: database port
        :param pgdatabase: database name
        :param pguser: database user
        :param pgpassword: database password
        """
        self.pghost = pghost
        self.pgport = pgport
        self.pgdatabase = pgdatabase
        self.pguser = pguser
        self.pgpassword = pgpassword

    def find_coupled_tem(self):
        from functions import DatabaseFunctions as Df, DatabaseConnection as Dc

        def unique(non_unique_list):
            unique_list = []
            for x in non_unique_list:
                if x not in unique_list:
                    unique_list.append(x)
            return unique_list

        # load data from database
        print('Traversing through GERDA database for TEM dataset...')
        sql_file = './sql/get_tem_dbdt.sql'
        sql = Dc.load_sql(sql_file)
        df_temwave = Df(self.pghost, self.pgport, self.pgdatabase, self.pguser, self.pgpassword).import_db_to_df(sql)

        # group data by dataset-id, position-id and the magnetic moment (segment)
        tem_grouped = df_temwave.groupby(['dataset', 'daposition', 'segment'])
        unique_dataset = tem_grouped['dataset'].unique()
        print(f' - A total of {len(unique_dataset)} tem dataset was found ({len(tem_grouped)} datapoints)')

        # find tem soundings with missing data-points
        print()
        print('Finding tem sounding with missing datapoints...')
        dataset_error = []
        for name, group in tem_grouped:
            group.sort_values(by=['sequence'])
            tem_diff = group['abscival'].diff().diff()

            for i, num in enumerate(tem_diff):
                if num < 0:
                    dataset_error.append(name[0])

        tem_coupled = unique(dataset_error)
        print(f' - {len(tem_coupled)} tem dataset id was found missing datapoints in the sounding')

        return tem_coupled
